---
title: "Alpha Diversity"
author: "Laurent Cauquil"
output: 
  html_document:
    keep_md: yes
---




```r
library(tidyverse)
library(phyloseq)
```


## Phyloseq object

Let's import a phyloseq object in the workspace


```r
load("data/16S_phyloseq.rdata")
ls()
```

```
## [1] "data"
```

Let's take a look a the data


```r
data
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 498 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 498 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 498 tips and 497 internal nodes ]
```

## Alpha diversity indices

There are several indices available in the phyloseq package:

 - Observed
 - Chao1
 - ACE
 - Shannon
 - Simpson
 - InvSimpson
 - Fisher
 
Other indices can be found in different functions from the vegan package

### Computation of the most used indices

 - Observed: is the richness, i.e. the number of OTU observed
 - Chao1: is an abundance based estimator, it estimates the richness + the number of unobserved species
 - Shannon: gives the evenness of the species abundance distribution
 - InvSimpson: inverse probability that two sequences sampled at random come from the same species

Interpretation on the results:

 - Chao1 close to Richness → all species have been detected
 - Higher Shannon index → higher homogeneity → greater diversity
 - Greater invsimpson index → greater diversity 
Indices are computed on a rarefy count table


```r
data_raref <- rarefy_even_depth(data, rngseed = 12345)
```

```
## `set.seed(12345)` was used to initialize repeatable random subsampling.
```

```
## Please record this for your records so others can reproduce.
```

```
## Try `set.seed(12345); .Random.seed` for the full vector
```

```
## ...
```

```r
alpha_div_raref <- estimate_richness(data_raref, measures = c("Observed", "Chao1", "Shannon", "InvSimpson"))
summary(alpha_div_raref)
```

```
##     Observed         Chao1          se.chao1         Shannon      
##  Min.   : 30.0   Min.   : 39.0   Min.   : 0.000   Min.   :0.9763  
##  1st Qu.:120.8   1st Qu.:123.5   1st Qu.: 4.118   1st Qu.:2.1830  
##  Median :144.0   Median :154.3   Median : 5.555   Median :2.7997  
##  Mean   :143.1   Mean   :153.2   Mean   : 6.025   Mean   :2.6514  
##  3rd Qu.:169.0   3rd Qu.:178.9   3rd Qu.: 7.791   3rd Qu.:3.3091  
##  Max.   :244.0   Max.   :258.3   Max.   :14.641   Max.   :4.3342  
##    InvSimpson    
##  Min.   : 1.656  
##  1st Qu.: 3.494  
##  Median : 6.195  
##  Mean   : 8.368  
##  3rd Qu.:11.836  
##  Max.   :32.512
```

## Visualization

### Visualization with boxplot()

**Visualization with plot_richness() function from phyloseq package**


```r
plot_richness(data, x = "EnvType", color = "EnvType", measures = c("Observed", "Chao1", "Shannon", "InvSimpson"))
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

**Adding boxplot**


```r
plot_richness(data, x = "EnvType", color = "EnvType", measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) +
  geom_boxplot()
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

**Adding values**


```r
plot_richness(data, x = "EnvType", color = "EnvType", measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) +
  geom_boxplot() +
  geom_point()
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

### Visualization without phyloseq

**Tidy data**


```r
df_alpha_div <- merge(sample_data(data_raref), alpha_div_raref, by = "row.names")
tmp <- select(df_alpha_div, !se.chao1)

tmp |> 
  pivot_longer(cols = Observed:InvSimpson, names_to = "Index", values_to = "Measure") -> df_alpha_div
```

**Visualization**


```r
correct_order <- c("Observed", "Chao1", "Shannon", "InvSimpson")
df_alpha_div |>
  ggplot(aes(x = EnvType, y = round(Measure,2), color = EnvType)) +
  geom_boxplot(aes(group = EnvType)) +
  geom_point() +
  facet_wrap(~ factor(Index, level = correct_order), nrow = 1, scales = "free_y") +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(title = "Boxplot and points of index of each Index by EnvType") +
  labs(y = "Alpha Diversity Measure")
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

### Visualization with barplot() when not enough samples for boxplot presentation

**Height of the bars represent the mean of the index with sd bar error**


```r
df_alpha_div |>
  group_by(Index, EnvType) |> 
  summarise(mean = mean(Measure),
            sd = sd(Measure)) |> 
  ggplot(aes(x = EnvType, y = mean, fill = EnvType)) +
  geom_col() +
  geom_errorbar(aes(ymin = mean - sd, ymax = mean + sd), width = .2) +
  facet_wrap(~ factor(Index, level = correct_order), nrow = 1, scales = "free_y") +
  theme(axis.text.x = element_text(angle = 90)) +
  labs(title = "Barplot of the mean of each Index by EnvType with error bar of sd") +
  labs(y = "Alpha Diversity Measure")
```

```
## `summarise()` has grouped output by 'Index'. You can override using the `.groups` argument.
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-10-1.png)<!-- -->



```r
df_alpha_div |>
  filter(EnvType == "GroundBeef", Index == "InvSimpson") |> 
  ggplot(aes(x = SampleID, y = Measure, fill = SampleID)) +
  geom_col(fill = "#3BCC86") +
  geom_text(aes(label = round(Measure,2)),   ## adds text directly to the plot
            vjust = 1, ## position from top to bottom of bar
            nudge_y = -.5, ## space between end of bar and label
            size = 4, 
            fontface = "bold", 
            family = "TT Arail") +
  theme(axis.text.x = element_text(angle = 75,
                                   vjust = 0.3)) +
  labs(title = "Barplot of InvSimpson index for GroundBeef") +
  labs(x = "DicedBacon", 
       y = "Alpha Diversity Measure")
```

```
## Warning in grid.Call.graphics(C_text, as.graphicsAnnot(x$label), x$x, x$y, :
## famille de police introuvable dans la base de données des polices Windows
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-11-1.png)<!-- -->


```r
df_alpha_div |>
  filter(EnvType == "CookedShrimp", Index == "InvSimpson") |> 
  ggplot(aes(x = SampleID, y = Measure, fill = SampleID)) +
  geom_col(fill = "#3BCC86") +
  ylim(0, 40) +  ## enlarge y axis because of label position on top
  geom_label(aes(label = round(Measure,2)), ## draws a rectangle behind the tex
             vjust = -0.5, ## position from top to bottom of bar
             nudge_y = -.5, ## space between end of bar and label
             size = 4, 
             fontface = "bold", 
             family = "serif", ## turn into white box without outline
             fill = "white", 
             col = "black", 
             label.size = .5) +  ## thickness of borderline of box
  theme(axis.text.x = element_text(angle = 75,
                                   vjust = 0)) +
  labs(title = "Barplot of InvSimpson index for CookedShrimp") +
  labs(x = "DicedBacon", 
       y = "Alpha Diversity Measure")
```

![](03_alpha_diversity_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

