---
title: "Taxonomy description"
author: "Laurent Cauquil"
output: 
  html_document:
    keep_md: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r, message = F, warning = F}
library(dplyr)
library(tibble)
library(ggplot2)
library(ggtree) ## load before phyloseq
library(phyloseq)
library(tidyr)
library(tidytree)
library(sunburstR)
```


## Phyloseq object

Let's import a phyloseq object in the workspace

```{r}
load("data/16S_phyloseq.rdata")
ls()
```

Let's take a look a the data

```{r}
data
```

## Construct tidy data

### Group taxonomy by Genus

```{r}
## Group by Genus and normalisation
data %>% 
  tax_glom(taxrank = "Genus") %>% 
  transform_sample_counts(function(OTU) OTU/sum(OTU)) -> data_genus
```

### Tidying

```{r}
TA_genus<- tibble::rownames_to_column(data.frame(otu_table(data_genus)), var = "Cluster")
tax_genus <- tibble::rownames_to_column(data.frame(tax_table(data_genus)@.Data), var = "Cluster")
meta_genus <-data.frame(sample_data(data_genus))

tmp <- left_join(tax_genus, TA_genus)
tmp |> 
  pivot_longer(cols = contains("LOT"), names_to = "SampleID", values_to = "Abundance") -> tmp2
data_tidy <- left_join(tmp2, meta_genus)
data_tidy$Kingdom |> 
  unique()
data_tidy |> 
  select(-c(Kingdom, Species)) -> data_tidy
rm(tmp, tmp2)
```

## Visualization Phylum by EnvType

Sum of relative abundance of all 8 samples for each EnvType

```{r}
data_tidy |> 
  group_by(EnvType, Phylum) |> 
  summarise(som = sum(Abundance), .groups = "keep") |> 
  ggplot(aes(x = EnvType, 
             y = som, 
             fill = Phylum)) + 
  geom_col() +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Phylum",
       title = "Relative abundance of Phylum by EnvType factor") +
  theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1))
```

## Visualization Phylum for each sample by EnvType

```{r,  fig.width = 12, fig.height = 8}
correct_order <- c("GroundBeef", "GroundVeal", "DicedBacon", "PoultrySausage",
                   "CookedShrimp", "CodFillet", "SalmonFillet", "SmokedSalmon")

data_tidy |> 
  ggplot(aes(x = SampleID, 
             y = Abundance, 
             fill = Phylum, 
             color = Phylum)) + 
  geom_col() +
  facet_wrap(~ factor(EnvType, level = correct_order), nrow = 1, scales = "free_x") +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Phylum",
       title = "Relative abundance of Phylum by EnvType factor") +
  theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1))
```

## Visualization top 10 Family for each sample by EnvType

Select top 10 Family

```{r,  fig.width = 12, fig.height = 8}
top_10_fam <- data_tidy %>% 
  group_by(Family) %>% 
  summarise(som = sum(Abundance)) %>% 
  top_n(10) %>% 
  select(Family) %>% 
  data.frame

data_tidy %>% 
  dplyr::filter(Family %in% as.character(top_10_fam[,1])) %>% 
  ggplot(aes(x = SampleID, 
             y = Abundance, fill = Family)) + 
  geom_col() + 
  facet_wrap(~ EnvType, nrow = 1, scales = "free_x") +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Family",
       title = "Relative abundance of top 10 Family by EnvType",
       subtitle = "Top 10") +
  theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1))
rm(top_10_fam)
```


## Top 10 Family distribution from Firmicutes Phylum

```{r, fig.width = 12, fig.height = 8}
top_10_fam_Firm <- data_tidy |>
  filter(Phylum == "Firmicutes") |> 
  group_by(Family) |> 
  summarise(som = sum(Abundance)) |> 
  top_n(10)

data_tidy |> 
  filter(Family %in% top_10_fam_Firm$Family) |> 
  ggplot(aes(x = SampleID, y = Abundance, fill = Family, color = Family)) +
  geom_col() +
  facet_wrap(~ EnvType, nrow = 1, scales = "free_x") +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Family",
       title = "Relative abundance of top 10 Family by EnvType from Firmicutes Phylum") +
  theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1))
rm(top_10_fam_Firm)
```



## Sunburst

```{r}
data_tidy[, c("Phylum", "Class", "Order", "Family", "Genus")] <- data_tidy |> 
  select(Phylum:Genus) |> 
  apply(2, function(x) gsub("-", "_", x))

data_tidy |> 
  unite("taxo", Phylum:Genus, sep = "-") |> 
  group_by(taxo) |> 
  summarise(som = sum(Abundance)) |> 
  select(taxo, som) -> ttt

sunburst(ttt)

```

```{r}
sund2b(ttt)
```

## Tree with ggtree package

```{r}
data_tree <- phy_tree(data_genus)
data_tree |> 
  ggtree(layout = "circular")
```







