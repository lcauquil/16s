---
title: "Transformations of the table count"
author: "Laurent Cauquil"
output: 
  html_document:
    keep_md: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r, message = F, warning = F}
library(tidyverse)
library(phyloseq)
library(metagenomeSeq) ## CSS normalisation
library(ape) ## fct read.tree()
library(psych)
library(vegan) ## fct wisconsin()
library(zCompositions) ## imput 0's
```


## Phyloseq object

Let's import a phyloseq object in the workspace

```{r}
load("data/16S_phyloseq.rdata")
ls()
```

Let's take a look a the data

```{r}
data
```

## Imputing zeros (GBM)

**Imputing zeros in compositional count data sets based on a Bayesian-multiplicative replacement**

```{r}
## library zCompositions)
data_gbm <- cmultRepl(data.frame(otu_table(data)), label = 0, method = "GBM", output = "p-counts")
```

## Transforming count table

### Rarefying

Rarefying is often use to eliminate the differences in depth sequencing. The function rarefy_even_depth() resample from the abundance value

```{r}
## rngseed used to fix a seed to make the rarefaction reproducible
data_raref <- rarefy_even_depth(data, rngseed = 12345)
unique(sample_sums(data_raref))
```

### Custom transformations

**Proportion or TSS normalisation**

```{r}
count_to_TSS <- function(x) {return(x / sum(x)) }
data_TSS <- transform_sample_counts(data, count_to_TSS)
rm(count_to_TSS)
```

**Centered Log Ratio transformation (CLR)**

```{r}
data_add_1 <- function(x) {return(x + 1)}
tmp <- transform_sample_counts(data, data_add_1)
count_to_CLR <- function(x) { return(log(x/psych::geometric.mean(x)))}
data_CLR <- transform_sample_counts(tmp, count_to_CLR) # add 1 because of the log(0)
otu_table(data_CLR) <- otu_table(data_CLR) + min(otu_table(data_CLR)) * -1 # *-1 because of negative number (log(x<1))
rm(data_add_1, count_to_CLR, tmp)
```

**TSS & Centered Log Ratio transformation**

```{r}
data_add_min <- function(x) {return(x + tmp)}

## define min value other than 0 then half it
tmp <- min(as.vector(otu_table(data_TSS))[which(otu_table(data_TSS) != 0)])/2
## add min to the table
tmp2 <- transform_sample_counts(data_TSS, data_add_min)
## apply CLR
count_to_CLR <- function(x) { return(log(x/psych::geometric.mean(x)))}
data_TSSCLR <- transform_sample_counts(tmp2, count_to_CLR)
rm(data_add_min, tmp, tmp2, count_to_CLR)
```

**Cumulative Sum Scaling CSS normalisation**

```{r, message = F, warning = F}
Y <- t(data.frame(otu_table(data)) + 1)

## calculate the proper percentile by which to normalize counts
data.metagenomeSeq <- newMRexperiment(Y, phenoData=NULL, featureData=NULL, libSize=NULL, normFactors=NULL)
data.cumnorm <- cumNorm(data.metagenomeSeq, p = cumNormStat(data.metagenomeSeq)) # Default value being used p = 0.5
tmp <- t(MRcounts(data.cumnorm, norm=TRUE, log=TRUE)) # the log transformation is applied implicitely when using the metagenomeSeq package

data_CSS <- phyloseq(otu_table(tmp, taxa_are_rows = T),
                     sample_data(read.csv("data/16S_metadata.tabular", sep = "", row.names = 1)),
                     phy_tree(read.tree("data/16S_tree.nhx")))

rm(tmp, Y, data.metagenomeSeq, data.cumnorm)
```

**Winsconsin transformation**

```{r}
tmp <- wisconsin(t(otu_table(data)))
data_wins <- phyloseq(otu_table(t(tmp), taxa_are_rows = T),
                      sample_data(read.csv("data/16S_metadata.tabular", sep = "", row.names = 1)),
                      phy_tree(read.tree("data/16S_tree.nhx")))
rm(tmp)
```

**Square root of square root**

```{r}
sqrt_sqrt <- function(x) {return(sqrt(sqrt(x))) }
data_sqrt_sqrt <- transform_sample_counts(data, sqrt_sqrt)
rm(sqrt_sqrt)
```

