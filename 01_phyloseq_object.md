---
title: "Structure and manipulation of the phyloseq object"
author: "Laurent Cauquil"
output: 
  html_document:
    keep_md: yes
---




```r
library(tidyverse)
library(phyloseq)
```


## Phyloseq object

Let's import a phyloseq object in the workspace

```r
load("data/16S_phyloseq.rdata")
ls()
```

```
## [1] "data"
```

Let's take a look a the data


```r
data
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 498 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 498 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 498 tips and 497 internal nodes ]
```

They are 4 different datasets included in the object data. Each of this objects can be reached using the functions mentioned at the beginning of each line.
 - otu_table() extract the count table
 - sample_data() extract the meta data linked to the samples
 - tax_table() extract the taxonomy of each OTU
 - phy_tree() extract the phylogenetic tree of the OTU

### otu_table()

otu_table() extract the count table of OTU among the samples, so we can note that among the 64 samples there is 498 OTU.
It's a big table, it's not pertinent to open it in the console, but you can open it in a viewer tab.


```r
View(otu_table(data))
```

Be aware that is not a data frame, it's a matrix.


```r
is.matrix(otu_table(data))
```

```
## [1] TRUE
```

```r
df_otu <- data.frame(otu_table(data))
```

Unlike what is usually done observations (samples) are in columns


```r
dim(df_otu)
```

```
## [1] 498  64
```

```r
colnames(df_otu)
```

```
##  [1] "BHT0.LOT01" "BHT0.LOT03" "BHT0.LOT04" "BHT0.LOT05" "BHT0.LOT06"
##  [6] "BHT0.LOT07" "BHT0.LOT08" "BHT0.LOT10" "CDT0.LOT02" "CDT0.LOT04"
## [11] "CDT0.LOT05" "CDT0.LOT06" "CDT0.LOT07" "CDT0.LOT08" "CDT0.LOT09"
## [16] "CDT0.LOT10" "DLT0.LOT01" "DLT0.LOT03" "DLT0.LOT04" "DLT0.LOT05"
## [21] "DLT0.LOT06" "DLT0.LOT07" "DLT0.LOT08" "DLT0.LOT10" "FCT0.LOT01"
## [26] "FCT0.LOT02" "FCT0.LOT03" "FCT0.LOT05" "FCT0.LOT06" "FCT0.LOT07"
## [31] "FCT0.LOT08" "FCT0.LOT10" "FST0.LOT01" "FST0.LOT02" "FST0.LOT03"
## [36] "FST0.LOT05" "FST0.LOT06" "FST0.LOT07" "FST0.LOT08" "FST0.LOT10"
## [41] "MVT0.LOT01" "MVT0.LOT03" "MVT0.LOT05" "MVT0.LOT06" "MVT0.LOT07"
## [46] "MVT0.LOT08" "MVT0.LOT09" "MVT0.LOT10" "SFT0.LOT01" "SFT0.LOT02"
## [51] "SFT0.LOT03" "SFT0.LOT04" "SFT0.LOT05" "SFT0.LOT06" "SFT0.LOT07"
## [56] "SFT0.LOT08" "VHT0.LOT01" "VHT0.LOT02" "VHT0.LOT03" "VHT0.LOT04"
## [61] "VHT0.LOT06" "VHT0.LOT07" "VHT0.LOT08" "VHT0.LOT10"
```

As usual there is a lot "0" in the table


```r
round(sum(df_otu == 0) / sum(!is.na(df_otu)) * 100, 2)
```

```
## [1] 68.97
```

Some OTU are present in all samples and others in very few


```r
range(apply(df_otu, 1, function(x) sum(x != 0)))
```

```
## [1]  2 64
```

Range of sequencing depth, sample_sums is a function of the phyloseq package so it's applied directly to the phyloseq object


```r
range(sample_sums(data))
```

```
## [1] 8293 9048
```

Range of otu abundance


```r
range(taxa_sums(data))
```

```
## [1]    28 84846
```

### sample_table()

sample_data() extract the meta data linked to the samples and **it's** a data frame


```r
head(sample_data(data))
```

```
##               EnvType Description FoodType   SampleID
## BHT0.LOT01 GroundBeef        LOT1     Meat BHT0.LOT01
## BHT0.LOT03 GroundBeef        LOT3     Meat BHT0.LOT03
## BHT0.LOT04 GroundBeef        LOT4     Meat BHT0.LOT04
## BHT0.LOT05 GroundBeef        LOT5     Meat BHT0.LOT05
## BHT0.LOT06 GroundBeef        LOT6     Meat BHT0.LOT06
## BHT0.LOT07 GroundBeef        LOT7     Meat BHT0.LOT07
```

```r
is.data.frame(sample_data(data))
```

```
## [1] TRUE
```


```r
summary(sample_data(data))
```

```
##            EnvType    Description    FoodType        SampleID 
##  DicedBacon    : 8   LOT6   : 8   Meat   :32   DLT0.LOT01: 1  
##  PoultrySausage: 8   LOT7   : 8   Seafood:32   DLT0.LOT03: 1  
##  GroundBeef    : 8   LOT8   : 8                DLT0.LOT04: 1  
##  GroundVeal    : 8   LOT1   : 7                DLT0.LOT05: 1  
##  SmokedSalmon  : 8   LOT3   : 7                DLT0.LOT06: 1  
##  SalmonFillet  : 8   LOT5   : 7                DLT0.LOT07: 1  
##  (Other)       :16   (Other):19                (Other)   :58
```


### tax_table() extract the taxonomy of each OTU

Be aware that is not a data frame, it's a matrix.


```r
head(tax_table(data))
```

```
## Taxonomy Table:     [6 taxa by 7 taxonomic ranks]:
##             Kingdom    Phylum         Class         Order               
## Cluster_282 "Bacteria" "Bacteroidota" "Bacteroidia" "Sphingobacteriales"
## Cluster_398 "Bacteria" "Bacteroidota" "Bacteroidia" "Sphingobacteriales"
## Cluster_489 "Bacteria" "Bacteroidota" "Bacteroidia" "Sphingobacteriales"
## Cluster_133 "Bacteria" "Bacteroidota" "Bacteroidia" "Sphingobacteriales"
## Cluster_446 "Bacteria" "Bacteroidota" "Bacteroidia" "Sphingobacteriales"
## Cluster_481 "Bacteria" "Bacteroidota" "Bacteroidia" "Sphingobacteriales"
##             Family                Genus             
## Cluster_282 "Sphingobacteriaceae" "Sphingobacterium"
## Cluster_398 "Sphingobacteriaceae" "Sphingobacterium"
## Cluster_489 "Sphingobacteriaceae" "Sphingobacterium"
## Cluster_133 "Sphingobacteriaceae" "Sphingobacterium"
## Cluster_446 "Sphingobacteriaceae" "Sphingobacterium"
## Cluster_481 "Sphingobacteriaceae" "Pedobacter"      
##             Species                                                        
## Cluster_282 "Sphingobacterium faecium"                                     
## Cluster_398 "Sphingobacterium anhuiense"                                   
## Cluster_489 "Sphingobacterium multivorum"                                  
## Cluster_133 "Sphingobacterium composti Ten et al. 2007 non Yoo et al. 2007"
## Cluster_446 "Sphingobacterium mizutaii"                                    
## Cluster_481 "Pedobacter heparinus"
```

```r
is.matrix(tax_table(data))
```

```
## [1] TRUE
```

## phy_tree() extract the phylogenetic tree of the OTU

The object extracted from the tree of the phyloseq object is a list


```r
str(phy_tree(data))
```

```
## List of 5
##  $ edge       : int [1:994, 1:2] 499 500 501 502 503 504 505 506 507 508 ...
##  $ Nnode      : int 497
##  $ tip.label  : chr [1:498] "Cluster_282" "Cluster_398" "Cluster_489" "Cluster_133" ...
##  $ edge.length: num [1:994] 0.0408 0.0458 0.0346 0.0411 0.0416 ...
##  $ node.label : chr [1:497] "NA" "0.993" "0.749" "0.872" ...
##  - attr(*, "class")= chr "phylo"
##  - attr(*, "order")= chr "cladewise"
```

It can be simply ploted with the plot function()


```r
plot(phy_tree(data))
```

![](01_phyloseq_object_files/figure-html/unnamed-chunk-15-1.png)<!-- -->

## Phyloseq functions

Some functions manipulate and filter the phyloseq object, they are very useful because they update all datasets in the phyloseq object when one of them is modified.

### prune_samples() and prune_taxa()

These 3 functions filter unwanted samples or taxa by defining those we want to keep using a vector

Keeping only samples that are meat products


```r
## vector of samples names to keep
SamplesToKeep <- sample_names(data)[sample_data(data)$FoodType == "Meat"]
df_meat <- prune_samples(samples = SamplesToKeep, x= data)
df_meat
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 498 taxa and 32 samples ]
## sample_data() Sample Data:       [ 32 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 498 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 498 tips and 497 internal nodes ]
```


```r
summary(sample_data(df_meat))
```

```
##            EnvType   Description FoodType        SampleID 
##  DicedBacon    :8   LOT1   :4    Meat:32   DLT0.LOT01: 1  
##  PoultrySausage:8   LOT3   :4              DLT0.LOT03: 1  
##  GroundBeef    :8   LOT6   :4              DLT0.LOT04: 1  
##  GroundVeal    :8   LOT7   :4              DLT0.LOT05: 1  
##                     LOT8   :4              DLT0.LOT06: 1  
##                     LOT10  :4              DLT0.LOT07: 1  
##                     (Other):8              (Other)   :26
```

Keeping only otu with at least 1000 sequences


```r
df_OTU_1000 <- prune_taxa(taxa_sums(data) > 1000, data)
df_OTU_1000
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 66 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 66 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 66 tips and 65 internal nodes ]
```


### subset_samples() and subset_taxa()

These 3 functions filter unwanted samples or taxa by defining those we want to keep using a condition

To keep only samples that are meat products


```r
## vector of samples names to keep
df_ground_meat <- subset_samples(physeq = data, EnvType %in% c("GroundBeef", "GroundVeal"))
df_ground_meat
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 498 taxa and 16 samples ]
## sample_data() Sample Data:       [ 16 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 498 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 498 tips and 497 internal nodes ]
```


```r
summary(sample_data(df_ground_meat))
```

```
##        EnvType   Description FoodType        SampleID 
##  GroundBeef:8   LOT1   :2    Meat:16   BHT0.LOT01: 1  
##  GroundVeal:8   LOT3   :2              BHT0.LOT03: 1  
##                 LOT4   :2              BHT0.LOT04: 1  
##                 LOT6   :2              BHT0.LOT05: 1  
##                 LOT7   :2              BHT0.LOT06: 1  
##                 LOT8   :2              BHT0.LOT07: 1  
##                 (Other):4              (Other)   :10
```

Keeping only taxa of the phylum *Firmicutes*


```r
df_firmicutes <- subset_taxa(data, Phylum == "Firmicutes")
df_firmicutes
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 166 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 166 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 166 tips and 165 internal nodes ]
```

```r
unique(tax_table(data)[ , "Phylum"])
```

```
## Taxonomy Table:     [10 taxa by 1 taxonomic ranks]:
##             Phylum            
## Cluster_282 "Bacteroidota"    
## Cluster_474 "Proteobacteria"  
## Cluster_215 "Campylobacterota"
## Cluster_335 "Patescibacteria" 
## Cluster_350 "Actinobacteriota"
## Cluster_319 "Desulfobacterota"
## Cluster_48  "Cyanobacteria"   
## Cluster_135 "Spirochaetota"   
## Cluster_458 "Firmicutes"      
## Cluster_46  "Fusobacteriota"
```

```r
unique(tax_table(df_firmicutes)[ , "Phylum"])
```

```
## Taxonomy Table:     [1 taxa by 1 taxonomic ranks]:
##             Phylum      
## Cluster_458 "Firmicutes"
```

Combining multiple conditions


```r
df_multiple <- subset_taxa(data, Phylum == "Firmicutes" & Class == "Bacilli")
df_multiple
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 126 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 126 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 126 tips and 125 internal nodes ]
```

```r
unique(tax_table(df_multiple)[ , c("Phylum", "Class")])
```

```
## Taxonomy Table:     [1 taxa by 2 taxonomic ranks]:
##             Phylum       Class    
## Cluster_443 "Firmicutes" "Bacilli"
```

### tax_glom()

This is one of the most powerful function of phyloseq.  
This method merges species that have the same taxonomy at a certain taxonomic rank.

Agglomerates otu to Class


```r
df_class <- tax_glom(data, taxrank = "Class")
df_class
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 17 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 17 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 17 tips and 16 internal nodes ]
```

All the information after the Class rank is lost


```r
tax_table(df_class)
```

```
## Taxonomy Table:     [17 taxa by 7 taxonomic ranks]:
##             Kingdom    Phylum             Class                 Order Family
## Cluster_20  "Bacteria" "Bacteroidota"     "Bacteroidia"         NA    NA    
## Cluster_3   "Bacteria" "Proteobacteria"   "Gammaproteobacteria" NA    NA    
## Cluster_39  "Bacteria" "Proteobacteria"   "Alphaproteobacteria" NA    NA    
## Cluster_215 "Bacteria" "Campylobacterota" "Campylobacteria"     NA    NA    
## Cluster_335 "Bacteria" "Patescibacteria"  "Gracilibacteria"     NA    NA    
## Cluster_350 "Bacteria" "Actinobacteriota" "Acidimicrobiia"      NA    NA    
## Cluster_256 "Bacteria" "Actinobacteriota" "Coriobacteriia"      NA    NA    
## Cluster_5   "Bacteria" "Actinobacteriota" "Actinobacteria"      NA    NA    
## Cluster_276 "Bacteria" "Patescibacteria"  "Saccharimonadia"     NA    NA    
## Cluster_319 "Bacteria" "Desulfobacterota" "Desulfobacteria"     NA    NA    
## Cluster_275 "Bacteria" "Desulfobacterota" "Desulfobulbia"       NA    NA    
## Cluster_48  "Bacteria" "Cyanobacteria"    "Cyanobacteriia"      NA    NA    
## Cluster_135 "Bacteria" "Spirochaetota"    "Spirochaetia"        NA    NA    
## Cluster_23  "Bacteria" "Fusobacteriota"   "Fusobacteriia"       NA    NA    
## Cluster_56  "Bacteria" "Firmicutes"       "Clostridia"          NA    NA    
## Cluster_245 "Bacteria" "Firmicutes"       "Negativicutes"       NA    NA    
## Cluster_1   "Bacteria" "Firmicutes"       "Bacilli"             NA    NA    
##             Genus Species
## Cluster_20  NA    NA     
## Cluster_3   NA    NA     
## Cluster_39  NA    NA     
## Cluster_215 NA    NA     
## Cluster_335 NA    NA     
## Cluster_350 NA    NA     
## Cluster_256 NA    NA     
## Cluster_5   NA    NA     
## Cluster_276 NA    NA     
## Cluster_319 NA    NA     
## Cluster_275 NA    NA     
## Cluster_48  NA    NA     
## Cluster_135 NA    NA     
## Cluster_23  NA    NA     
## Cluster_56  NA    NA     
## Cluster_245 NA    NA     
## Cluster_1   NA    NA
```

