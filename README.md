# 16S

[Phyloseq structure and manipulating](01_phyloseq_object.md)  

[Count table transformations](02_count_table_transformations.md)  

[Alpha diversity](03_alpha_diversity.md)  

[Beta diversity visualization](04_beta_diversity_visualization.md)  

[Taxonomy](06_taxonomy.md)
