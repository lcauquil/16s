---
title: "Taxonomy description"
author: "Laurent Cauquil"
output: 
  html_document:
    keep_md: yes
---




```r
library(dplyr)
library(tibble)
library(ggplot2)
library(ggtree) ## load before phyloseq
library(phyloseq)
library(tidyr)
library(tidytree)
library(sunburstR)
```


## Phyloseq object

Let's import a phyloseq object in the workspace


```r
load("data/16S_phyloseq.rdata")
ls()
```

```
## [1] "data"
```

Let's take a look a the data


```r
data
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 498 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 498 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 498 tips and 497 internal nodes ]
```

## Construct tidy data

### Group taxonomy by Genus


```r
## Group by Genus and normalisation
data %>% 
  tax_glom(taxrank = "Genus") %>% 
  transform_sample_counts(function(OTU) OTU/sum(OTU)) -> data_genus
```

### Tidying


```r
TA_genus<- tibble::rownames_to_column(data.frame(otu_table(data_genus)), var = "Cluster")
tax_genus <- tibble::rownames_to_column(data.frame(tax_table(data_genus)@.Data), var = "Cluster")
meta_genus <-data.frame(sample_data(data_genus))

tmp <- left_join(tax_genus, TA_genus)
```

```
## Joining, by = "Cluster"
```

```r
tmp |> 
  pivot_longer(cols = contains("LOT"), names_to = "SampleID", values_to = "Abundance") -> tmp2
data_tidy <- left_join(tmp2, meta_genus)
```

```
## Joining, by = "SampleID"
```

```r
data_tidy$Kingdom |> 
  unique()
```

```
## [1] "Bacteria"
```

```r
data_tidy |> 
  select(-c(Kingdom, Species)) -> data_tidy
rm(tmp, tmp2)
```

## Visualization Phylum by EnvType

Sum of relative abundance of all 8 samples for each EnvType


```r
data_tidy |> 
  group_by(EnvType, Phylum) |> 
  summarise(som = sum(Abundance), .groups = "keep") |> 
  ggplot(aes(x = EnvType, 
             y = som, 
             fill = Phylum)) + 
  geom_col() +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Phylum",
       title = "Relative abundance of Phylum by EnvType factor") +
  theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1))
```

![](06_taxonomy_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

## Visualization Phylum for each sample by EnvType


```r
correct_order <- c("GroundBeef", "GroundVeal", "DicedBacon", "PoultrySausage",
                   "CookedShrimp", "CodFillet", "SalmonFillet", "SmokedSalmon")

data_tidy |> 
  ggplot(aes(x = SampleID, 
             y = Abundance, 
             fill = Phylum, 
             color = Phylum)) + 
  geom_col() +
  facet_wrap(~ factor(EnvType, level = correct_order), nrow = 1, scales = "free_x") +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Phylum",
       title = "Relative abundance of Phylum by EnvType factor") +
  theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1))
```

![](06_taxonomy_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

## Visualization top 10 Family for each sample by EnvType

Select top 10 Family


```r
top_10_fam <- data_tidy %>% 
  group_by(Family) %>% 
  summarise(som = sum(Abundance)) %>% 
  top_n(10) %>% 
  select(Family) %>% 
  data.frame
```

```
## Selecting by som
```

```r
data_tidy %>% 
  dplyr::filter(Family %in% as.character(top_10_fam[,1])) %>% 
  ggplot(aes(x = SampleID, 
             y = Abundance, fill = Family)) + 
  geom_col() + 
  facet_wrap(~ EnvType, nrow = 1, scales = "free_x") +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Family",
       title = "Relative abundance of top 10 Family by EnvType",
       subtitle = "Top 10") +
  theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1))
```

![](06_taxonomy_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

```r
rm(top_10_fam)
```


## Top 10 Family distribution from Firmicutes Phylum


```r
top_10_fam_Firm <- data_tidy |>
  filter(Phylum == "Firmicutes") |> 
  group_by(Family) |> 
  summarise(som = sum(Abundance)) |> 
  top_n(10)
```

```
## Selecting by som
```

```r
data_tidy |> 
  filter(Family %in% top_10_fam_Firm$Family) |> 
  ggplot(aes(x = SampleID, y = Abundance, fill = Family, color = Family)) +
  geom_col() +
  facet_wrap(~ EnvType, nrow = 1, scales = "free_x") +
  theme_classic() + 
  xlab("EnvType") +
  ylab("Abundance") +
  labs(fill = "Family",
       title = "Relative abundance of top 10 Family by EnvType from Firmicutes Phylum") +
  theme(axis.text.x = element_text(angle = 90, vjust = 1, hjust = 1))
```

![](06_taxonomy_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

```r
rm(top_10_fam_Firm)
```



## Sunburst


```r
data_tidy[, c("Phylum", "Class", "Order", "Family", "Genus")] <- data_tidy |> 
  select(Phylum:Genus) |> 
  apply(2, function(x) gsub("-", "_", x))

data_tidy |> 
  unite("taxo", Phylum:Genus, sep = "-") |> 
  group_by(taxo) |> 
  summarise(som = sum(Abundance)) |> 
  select(taxo, som) -> ttt

sunburst(ttt)
```

```{=html}
<div class="sunburst html-widget" id="htmlwidget-951f85882e26d3c83390" style="width:672px;height:480px; position:relative;">
<div>
<div class="sunburst-main">
<div class="sunburst-sequence"></div>
<div class="sunburst-chart">
<div class="sunburst-explanation" style="visibility:hidden;"></div>
</div>
</div>
<div class="sunburst-sidebar">
<input type="checkbox" class="sunburst-togglelegend" style="visibility:hidden;">Legend</input>
<div class="sunburst-legend" style="visibility:hidden;"></div>
</div>
</div>
</div>
<script type="application/json" data-for="htmlwidget-951f85882e26d3c83390">{"x":{"data":{"children":[{"name":"Actinobacteriota","children":[{"name":"Acidimicrobiia","children":[{"name":"Actinomarinales","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0111,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Microtrichales","children":[{"name":"Ilumatobacteraceae","children":[{"name":"unknown genus","size":0.0163,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Actinobacteria","children":[{"name":"Actinomycetales","children":[{"name":"Actinomycetaceae","children":[{"name":"Actinomyces","size":0.0428,"colname":"X5"},{"name":"Trueperella","size":0.0853,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Corynebacteriales","children":[{"name":"Corynebacteriaceae","children":[{"name":"Corynebacterium","size":0.5989,"colname":"X5"}],"colname":"X4"},{"name":"Dietziaceae","children":[{"name":"Dietzia","size":0.0413,"colname":"X5"}],"colname":"X4"},{"name":"Mycobacteriaceae","children":[{"name":"Mycobacterium","size":0.0069,"colname":"X5"}],"colname":"X4"},{"name":"Nocardiaceae","children":[{"name":"Rhodococcus","size":0.1494,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Micrococcales","children":[{"name":"Brevibacteriaceae","children":[{"name":"Brevibacterium","size":0.0634,"colname":"X5"}],"colname":"X4"},{"name":"Demequinaceae","children":[{"name":"Demequina","size":0.0085,"colname":"X5"}],"colname":"X4"},{"name":"Dermabacteraceae","children":[{"name":"Brachybacterium","size":0.0256,"colname":"X5"}],"colname":"X4"},{"name":"Dermacoccaceae","children":[{"name":"Dermacoccus","size":0.0126,"colname":"X5"}],"colname":"X4"},{"name":"Microbacteriaceae","children":[{"name":"Frigoribacterium","size":0.0368,"colname":"X5"},{"name":"Leucobacter","size":0.164,"colname":"X5"},{"name":"Microbacterium","size":0.086,"colname":"X5"},{"name":"Pseudoclavibacter","size":0.0211,"colname":"X5"}],"colname":"X4"},{"name":"Micrococcaceae","children":[{"name":"Arthrobacter","size":0.4541,"colname":"X5"},{"name":"Glutamicibacter","size":0.1503,"colname":"X5"},{"name":"Kocuria","size":0.1183,"colname":"X5"},{"name":"Micrococcus","size":0.1241,"colname":"X5"},{"name":"Paenarthrobacter","size":0.0091,"colname":"X5"},{"name":"Paeniglutamicibacter","size":0.1224,"colname":"X5"},{"name":"Rothia","size":0.0467,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Propionibacteriales","children":[{"name":"Nocardioidaceae","children":[{"name":"Marmoricola","size":0.0179,"colname":"X5"},{"name":"Nocardioides","size":0.0044,"colname":"X5"}],"colname":"X4"},{"name":"Propionibacteriaceae","children":[{"name":"Cutibacterium","size":2.6151,"colname":"X5"},{"name":"Luteococcus","size":0.1767,"colname":"X5"},{"name":"Propionibacterium","size":0.0591,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Coriobacteriia","children":[{"name":"Coriobacteriales","children":[{"name":"Atopobiaceae","children":[{"name":"Olsenella","size":0.0385,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Bacteroidota","children":[{"name":"Bacteroidia","children":[{"name":"Bacteroidales","children":[{"name":"Bacteroidaceae","children":[{"name":"Bacteroides","size":0.0427,"colname":"X5"}],"colname":"X4"},{"name":"Porphyromonadaceae","children":[{"name":"Porphyromonas","size":0.0128,"colname":"X5"}],"colname":"X4"},{"name":"Prevotellaceae","children":[{"name":"Alloprevotella","size":0.0372,"colname":"X5"},{"name":"Prevotella","size":0.0334,"colname":"X5"},{"name":"Prevotella_7","size":0.0213,"colname":"X5"}],"colname":"X4"},{"name":"Rikenellaceae","children":[{"name":"Rikenellaceae RC9 gut group","size":0.1346,"colname":"X5"},{"name":"U29_B03","size":0.0131,"colname":"X5"}],"colname":"X4"},{"name":"Tannerellaceae","children":[{"name":"Macellibacteroides","size":0.0653,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Chitinophagales","children":[{"name":"Chitinophagaceae","children":[{"name":"Sediminibacterium","size":0.0173,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Flavobacteriales","children":[{"name":"Flavobacteriaceae","children":[{"name":"Flavobacterium","size":3.1099,"colname":"X5"},{"name":"Myroides","size":0.3609,"colname":"X5"},{"name":"Robiginitalea","size":0.0116,"colname":"X5"},{"name":"Spongiimonas","size":0.0298,"colname":"X5"}],"colname":"X4"},{"name":"Weeksellaceae","children":[{"name":"Chryseobacterium","size":2.0134,"colname":"X5"},{"name":"Cloacibacterium","size":0.0636,"colname":"X5"},{"name":"Empedobacter","size":0.4414,"colname":"X5"},{"name":"Riemerella","size":0.0093,"colname":"X5"},{"name":"Soonwooa","size":0.2171,"colname":"X5"},{"name":"unknown genus","size":0.0569,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Sphingobacteriales","children":[{"name":"Sphingobacteriaceae","children":[{"name":"Pedobacter","size":0.0062,"colname":"X5"},{"name":"Sphingobacterium","size":0.1125,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Campylobacterota","children":[{"name":"Campylobacteria","children":[{"name":"Campylobacterales","children":[{"name":"Arcobacteraceae","children":[{"name":"Arcobacter","size":0.0162,"colname":"X5"},{"name":"Pseudarcobacter","size":0.0307,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Cyanobacteria","children":[{"name":"Cyanobacteriia","children":[{"name":"Synechococcales","children":[{"name":"Cyanobiaceae","children":[{"name":"Cyanobium PCC_6307","size":0.1737,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Desulfobacterota","children":[{"name":"Desulfobacteria","children":[{"name":"Desulfobacterales","children":[{"name":"Desulfosarcinaceae","children":[{"name":"SEEP_SRB1","size":0.0166,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Desulfobulbia","children":[{"name":"Desulfobulbales","children":[{"name":"Desulfocapsaceae","children":[{"name":"unknown genus","size":0.0334,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Firmicutes","children":[{"name":"Bacilli","children":[{"name":"Bacillales","children":[{"name":"Bacillaceae","children":[{"name":"Anoxybacillus","size":0.0448,"colname":"X5"},{"name":"Bacillus","size":0.1103,"colname":"X5"},{"name":"Caldibacillus","size":0.0483,"colname":"X5"}],"colname":"X4"},{"name":"Planococcaceae","children":[{"name":"Chryseomicrobium","size":0.0134,"colname":"X5"},{"name":"Kurthia","size":0.0326,"colname":"X5"},{"name":"Planococcus","size":0.0675,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Erysipelotrichales","children":[{"name":"Erysipelatoclostridiaceae","children":[{"name":"Sharpea","size":0.0352,"colname":"X5"}],"colname":"X4"},{"name":"Erysipelotrichaceae","children":[{"name":"Erysipelothrix","size":0.0081,"colname":"X5"},{"name":"Faecalitalea","size":0.0259,"colname":"X5"},{"name":"Holdemanella","size":0.0438,"colname":"X5"},{"name":"Turicibacter","size":0.051,"colname":"X5"},{"name":"ZOR0006","size":1.7655,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Exiguobacterales","children":[{"name":"Exiguobacteraceae","children":[{"name":"Exiguobacterium","size":0.0674,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Lactobacillales","children":[{"name":"Aerococcaceae","children":[{"name":"Abiotrophia","size":0.0193,"colname":"X5"},{"name":"Aerococcus","size":0.0538,"colname":"X5"},{"name":"Facklamia","size":0.0323,"colname":"X5"},{"name":"Ignavigranum","size":0.0101,"colname":"X5"},{"name":"unknown genus","size":0.0743,"colname":"X5"}],"colname":"X4"},{"name":"Carnobacteriaceae","children":[{"name":"Atopostipes","size":0.0095,"colname":"X5"},{"name":"Carnobacterium","size":1.7918,"colname":"X5"},{"name":"Granulicatella","size":0.009,"colname":"X5"},{"name":"Jeotgalibaca","size":0.0439,"colname":"X5"},{"name":"Trichococcus","size":0.0345,"colname":"X5"}],"colname":"X4"},{"name":"Enterococcaceae","children":[{"name":"Enterococcus","size":0.3359,"colname":"X5"}],"colname":"X4"},{"name":"Lactobacillaceae","children":[{"name":"Companilactobacillus","size":0.0861,"colname":"X5"},{"name":"Dellaglioa","size":1.1364,"colname":"X5"},{"name":"Lacticaseibacillus","size":0.036,"colname":"X5"},{"name":"Lactiplantibacillus","size":0.0311,"colname":"X5"},{"name":"Lactobacillus","size":0.2276,"colname":"X5"},{"name":"Latilactobacillus","size":6.1807,"colname":"X5"},{"name":"Leuconostoc","size":4.7748,"colname":"X5"},{"name":"Levilactobacillus","size":0.0369,"colname":"X5"},{"name":"Ligilactobacillus","size":0.1269,"colname":"X5"},{"name":"Limosilactobacillus","size":0.0312,"colname":"X5"},{"name":"Paucilactobacillus","size":0.0347,"colname":"X5"},{"name":"Secundilactobacillus","size":0.012,"colname":"X5"},{"name":"Weissella","size":1.5345,"colname":"X5"}],"colname":"X4"},{"name":"Listeriaceae","children":[{"name":"Brochothrix","size":9.8222,"colname":"X5"},{"name":"Listeria","size":0.0746,"colname":"X5"}],"colname":"X4"},{"name":"Streptococcaceae","children":[{"name":"Lactococcus","size":4.4819,"colname":"X5"},{"name":"Streptococcus","size":1.6357,"colname":"X5"}],"colname":"X4"},{"name":"Vagococcaceae","children":[{"name":"Vagococcus","size":0.2352,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Mycoplasmatales","children":[{"name":"Mycoplasmataceae","children":[{"name":"Candidatus Bacilloplasma","size":0.4622,"colname":"X5"},{"name":"Mycoplasma","size":0.094,"colname":"X5"},{"name":"unknown genus","size":0.0152,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Paenibacillales","children":[{"name":"Paenibacillaceae","children":[{"name":"Paenibacillus","size":0.0425,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Staphylococcales","children":[{"name":"Gemellaceae","children":[{"name":"Gemella","size":0.0245,"colname":"X5"}],"colname":"X4"},{"name":"Staphylococcaceae","children":[{"name":"Jeotgalicoccus","size":0.0329,"colname":"X5"},{"name":"Macrococcus","size":0.5535,"colname":"X5"},{"name":"Nosocomiicoccus","size":0.0091,"colname":"X5"},{"name":"Staphylococcus","size":2.0317,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Clostridia","children":[{"name":"Christensenellales","children":[{"name":"Christensenellaceae","children":[{"name":"Christensenellaceae R_7 group","size":0.054,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Clostridia UCG_014","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0558,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Clostridiales","children":[{"name":"Clostridiaceae","children":[{"name":"Clostridium sensu stricto 1","size":0.0784,"colname":"X5"},{"name":"Clostridium sensu stricto 5","size":0.0069,"colname":"X5"},{"name":"Proteiniclasticum","size":0.0615,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Eubacteriales","children":[{"name":"Eubacteriaceae","children":[{"name":"Eubacterium","size":0.0078,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Lachnospirales","children":[{"name":"Lachnospiraceae","children":[{"name":"[Ruminococcus] gauvreauii group","size":0.0398,"colname":"X5"},{"name":"Blautia","size":0.0571,"colname":"X5"},{"name":"Butyrivibrio","size":0.0097,"colname":"X5"},{"name":"Dorea","size":0.0108,"colname":"X5"},{"name":"Lachnoclostridium","size":0.023,"colname":"X5"},{"name":"Lachnospiraceae NK3A20 group","size":0.0649,"colname":"X5"},{"name":"Syntrophococcus","size":0.0102,"colname":"X5"},{"name":"Tyzzerella","size":0.0691,"colname":"X5"},{"name":"unknown genus","size":0.0173,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Oscillospirales","children":[{"name":"Hungateiclostridiaceae","children":[{"name":"Saccharofermentans","size":0.019,"colname":"X5"}],"colname":"X4"},{"name":"Oscillospiraceae","children":[{"name":"UCG_005","size":0.0128,"colname":"X5"}],"colname":"X4"},{"name":"Ruminococcaceae","children":[{"name":"Faecalibacterium","size":0.0756,"colname":"X5"},{"name":"Ruminococcus","size":0.0136,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Peptostreptococcales_Tissierellales","children":[{"name":"Anaerovoracaceae","children":[{"name":"Mogibacterium","size":0.0084,"colname":"X5"}],"colname":"X4"},{"name":"Family XI","children":[{"name":"Anaerococcus","size":0.0202,"colname":"X5"},{"name":"Anaerosphaera","size":0.074,"colname":"X5"},{"name":"Finegoldia","size":0.0142,"colname":"X5"},{"name":"Gallicola","size":0.0182,"colname":"X5"},{"name":"Peptoniphilus","size":0.0933,"colname":"X5"},{"name":"Tissierella","size":0.0122,"colname":"X5"}],"colname":"X4"},{"name":"Peptostreptococcaceae","children":[{"name":"Intestinibacter","size":0.0253,"colname":"X5"},{"name":"Paraclostridium","size":0.0115,"colname":"X5"},{"name":"Peptostreptococcus","size":0.0409,"colname":"X5"},{"name":"Proteocatella","size":0.0037,"colname":"X5"},{"name":"unknown genus","size":0.1273,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Negativicutes","children":[{"name":"Acidaminococcales","children":[{"name":"Acidaminococcaceae","children":[{"name":"Succiniclasticum","size":0.0372,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Veillonellales_Selenomonadales","children":[{"name":"Veillonellaceae","children":[{"name":"Veillonella","size":0.0281,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Fusobacteriota","children":[{"name":"Fusobacteriia","children":[{"name":"Fusobacteriales","children":[{"name":"Fusobacteriaceae","children":[{"name":"Cetobacterium","size":0.2577,"colname":"X5"},{"name":"Fusobacterium","size":0.1245,"colname":"X5"}],"colname":"X4"},{"name":"Leptotrichiaceae","children":[{"name":"Hypnocyclicus","size":0.4276,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Patescibacteria","children":[{"name":"Gracilibacteria","children":[{"name":"Candidatus Peribacteria","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0146,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Saccharimonadia","children":[{"name":"Saccharimonadales","children":[{"name":"Saccharimonadaceae","children":[{"name":"Candidatus Saccharimonas","size":0.0195,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Proteobacteria","children":[{"name":"Alphaproteobacteria","children":[{"name":"Acetobacterales","children":[{"name":"Acetobacteraceae","children":[{"name":"Acidocella","size":0.0149,"colname":"X5"},{"name":"Roseomonas","size":0.1252,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Caulobacterales","children":[{"name":"Caulobacteraceae","children":[{"name":"Brevundimonas","size":0.1044,"colname":"X5"},{"name":"unknown genus","size":0.01,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Rhizobiales","children":[{"name":"Beijerinckiaceae","children":[{"name":"Methylobacterium_Methylorubrum","size":0.0316,"colname":"X5"}],"colname":"X4"},{"name":"Pleomorphomonadaceae","children":[{"name":"Pleomorphomonas","size":0.0161,"colname":"X5"}],"colname":"X4"},{"name":"Rhizobiaceae","children":[{"name":"Allorhizobium_Neorhizobium_Pararhizobium_Rhizobium","size":0.0438,"colname":"X5"},{"name":"Ochrobactrum","size":0.1527,"colname":"X5"},{"name":"Pseudochrobactrum","size":0.24,"colname":"X5"}],"colname":"X4"},{"name":"Stappiaceae","children":[{"name":"Roseibium","size":0.0076,"colname":"X5"}],"colname":"X4"},{"name":"Xanthobacteraceae","children":[{"name":"unknown genus","size":0.0814,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Rhodobacterales","children":[{"name":"Rhodobacteraceae","children":[{"name":"Paracoccus","size":0.1927,"colname":"X5"},{"name":"Rhodobacter","size":0.0178,"colname":"X5"},{"name":"Ruegeria","size":0.0412,"colname":"X5"},{"name":"Silicimonas","size":0.0096,"colname":"X5"},{"name":"Tabrizicola","size":0.0162,"colname":"X5"},{"name":"Tropicimonas","size":0.0296,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Sphingomonadales","children":[{"name":"Sphingomonadaceae","children":[{"name":"Rhizorhapis","size":0.0201,"colname":"X5"},{"name":"Sphingobium","size":0.0121,"colname":"X5"},{"name":"Sphingomonas","size":0.0383,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"unknown order","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0608,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Gammaproteobacteria","children":[{"name":"Burkholderiales","children":[{"name":"Burkholderiaceae","children":[{"name":"Burkholderia_Caballeronia_Paraburkholderia","size":0.0229,"colname":"X5"},{"name":"Limnobacter","size":0.072,"colname":"X5"},{"name":"Ralstonia","size":0.0178,"colname":"X5"}],"colname":"X4"},{"name":"Chitinimonadaceae","children":[{"name":"Chitinimonas","size":0.0141,"colname":"X5"}],"colname":"X4"},{"name":"Comamonadaceae","children":[{"name":"Acidovorax","size":0.086,"colname":"X5"},{"name":"Aquabacterium","size":0.0318,"colname":"X5"},{"name":"Comamonas","size":0.2547,"colname":"X5"},{"name":"Curvibacter","size":0.015,"colname":"X5"},{"name":"Delftia","size":0.0176,"colname":"X5"},{"name":"Limnohabitans","size":0.0208,"colname":"X5"},{"name":"Pelomonas","size":0.0361,"colname":"X5"},{"name":"Polaromonas","size":0.0086,"colname":"X5"},{"name":"Pseudorhodoferax","size":0.0088,"colname":"X5"},{"name":"Rhodoferax","size":0.0057,"colname":"X5"},{"name":"Schlegelella","size":0.0133,"colname":"X5"},{"name":"Simplicispira","size":0.013,"colname":"X5"},{"name":"unknown genus","size":0.0716,"colname":"X5"},{"name":"Variovorax","size":0.0104,"colname":"X5"},{"name":"Xylophilus","size":0.0061,"colname":"X5"}],"colname":"X4"},{"name":"Hydrogenophilaceae","children":[{"name":"Hydrogenophilus","size":0.3541,"colname":"X5"}],"colname":"X4"},{"name":"Neisseriaceae","children":[{"name":"Neisseria","size":0.0254,"colname":"X5"},{"name":"unknown genus","size":0.2024,"colname":"X5"},{"name":"Uruburuella","size":0.0262,"colname":"X5"},{"name":"Vitreoscilla","size":0.0049,"colname":"X5"}],"colname":"X4"},{"name":"Oxalobacteraceae","children":[{"name":"[Aquaspirillum] arcticum group","size":0.1715,"colname":"X5"},{"name":"Janthinobacterium","size":0.4745,"colname":"X5"},{"name":"Massilia","size":0.059,"colname":"X5"},{"name":"Undibacterium","size":0.0357,"colname":"X5"}],"colname":"X4"},{"name":"Rhodocyclaceae","children":[{"name":"Methyloversatilis","size":0.0083,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Chromatiales","children":[{"name":"Chromatiaceae","children":[{"name":"unknown genus","size":0.0096,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Enterobacterales","children":[{"name":"Aeromonadaceae","children":[{"name":"Aeromonas","size":0.0081,"colname":"X5"}],"colname":"X4"},{"name":"Enterobacteriaceae","children":[{"name":"Raoultella","size":0.0041,"colname":"X5"}],"colname":"X4"},{"name":"Hafniaceae","children":[{"name":"Hafnia_Obesumbacterium","size":0.0309,"colname":"X5"}],"colname":"X4"},{"name":"Morganellaceae","children":[{"name":"Morganella","size":0.0035,"colname":"X5"},{"name":"Proteus","size":0.0285,"colname":"X5"}],"colname":"X4"},{"name":"Pseudoalteromonadaceae","children":[{"name":"Pseudoalteromonas","size":0.0148,"colname":"X5"}],"colname":"X4"},{"name":"Shewanellaceae","children":[{"name":"Shewanella","size":0.1157,"colname":"X5"}],"colname":"X4"},{"name":"Vibrionaceae","children":[{"name":"Aliivibrio","size":0.1562,"colname":"X5"},{"name":"Photobacterium","size":5.1659,"colname":"X5"},{"name":"Vibrio","size":0.0395,"colname":"X5"}],"colname":"X4"},{"name":"Yersiniaceae","children":[{"name":"Serratia","size":0.1744,"colname":"X5"},{"name":"Yersinia","size":0.0166,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Milano_WF1B_44","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.1134,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Pseudomonadales","children":[{"name":"Moraxellaceae","children":[{"name":"Acinetobacter","size":0.261,"colname":"X5"},{"name":"Psychrobacter","size":0.5081,"colname":"X5"}],"colname":"X4"},{"name":"Pseudomonadaceae","children":[{"name":"Pseudomonas","size":0.5068,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"TDNP_Wbc97_11_7_16","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0305,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Xanthomonadales","children":[{"name":"Rhodanobacteraceae","children":[{"name":"Dyella","size":0.0362,"colname":"X5"},{"name":"Fulvimonas","size":0.1291,"colname":"X5"},{"name":"Rhodanobacter","size":0.0857,"colname":"X5"}],"colname":"X4"},{"name":"Xanthomonadaceae","children":[{"name":"Lysobacter","size":0.0076,"colname":"X5"},{"name":"Stenotrophomonas","size":0.1953,"colname":"X5"},{"name":"Xylella","size":0.0165,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Spirochaetota","children":[{"name":"Spirochaetia","children":[{"name":"Spirochaetales","children":[{"name":"Spirochaetaceae","children":[{"name":"unknown genus","size":0.062,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"}],"name":"root"},"options":{"legendOrder":null,"colors":null,"valueField":"size","percent":true,"count":false,"explanation":null,"breadcrumb":[],"legend":[],"sortFunction":null,"sumNodes":true}},"evals":[],"jsHooks":[]}</script>
```


```r
sund2b(ttt)
```

```{=html}
<div id="htmlwidget-4a1038e41d2dfa810b09" style="width:672px;height:480px;" class="sund2b html-widget"></div>
<script type="application/json" data-for="htmlwidget-4a1038e41d2dfa810b09">{"x":{"data":{"root":{"children":[{"name":"Actinobacteriota","children":[{"name":"Acidimicrobiia","children":[{"name":"Actinomarinales","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0111,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Microtrichales","children":[{"name":"Ilumatobacteraceae","children":[{"name":"unknown genus","size":0.0163,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Actinobacteria","children":[{"name":"Actinomycetales","children":[{"name":"Actinomycetaceae","children":[{"name":"Actinomyces","size":0.0428,"colname":"X5"},{"name":"Trueperella","size":0.0853,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Corynebacteriales","children":[{"name":"Corynebacteriaceae","children":[{"name":"Corynebacterium","size":0.5989,"colname":"X5"}],"colname":"X4"},{"name":"Dietziaceae","children":[{"name":"Dietzia","size":0.0413,"colname":"X5"}],"colname":"X4"},{"name":"Mycobacteriaceae","children":[{"name":"Mycobacterium","size":0.0069,"colname":"X5"}],"colname":"X4"},{"name":"Nocardiaceae","children":[{"name":"Rhodococcus","size":0.1494,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Micrococcales","children":[{"name":"Brevibacteriaceae","children":[{"name":"Brevibacterium","size":0.0634,"colname":"X5"}],"colname":"X4"},{"name":"Demequinaceae","children":[{"name":"Demequina","size":0.0085,"colname":"X5"}],"colname":"X4"},{"name":"Dermabacteraceae","children":[{"name":"Brachybacterium","size":0.0256,"colname":"X5"}],"colname":"X4"},{"name":"Dermacoccaceae","children":[{"name":"Dermacoccus","size":0.0126,"colname":"X5"}],"colname":"X4"},{"name":"Microbacteriaceae","children":[{"name":"Frigoribacterium","size":0.0368,"colname":"X5"},{"name":"Leucobacter","size":0.164,"colname":"X5"},{"name":"Microbacterium","size":0.086,"colname":"X5"},{"name":"Pseudoclavibacter","size":0.0211,"colname":"X5"}],"colname":"X4"},{"name":"Micrococcaceae","children":[{"name":"Arthrobacter","size":0.4541,"colname":"X5"},{"name":"Glutamicibacter","size":0.1503,"colname":"X5"},{"name":"Kocuria","size":0.1183,"colname":"X5"},{"name":"Micrococcus","size":0.1241,"colname":"X5"},{"name":"Paenarthrobacter","size":0.0091,"colname":"X5"},{"name":"Paeniglutamicibacter","size":0.1224,"colname":"X5"},{"name":"Rothia","size":0.0467,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Propionibacteriales","children":[{"name":"Nocardioidaceae","children":[{"name":"Marmoricola","size":0.0179,"colname":"X5"},{"name":"Nocardioides","size":0.0044,"colname":"X5"}],"colname":"X4"},{"name":"Propionibacteriaceae","children":[{"name":"Cutibacterium","size":2.6151,"colname":"X5"},{"name":"Luteococcus","size":0.1767,"colname":"X5"},{"name":"Propionibacterium","size":0.0591,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Coriobacteriia","children":[{"name":"Coriobacteriales","children":[{"name":"Atopobiaceae","children":[{"name":"Olsenella","size":0.0385,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Bacteroidota","children":[{"name":"Bacteroidia","children":[{"name":"Bacteroidales","children":[{"name":"Bacteroidaceae","children":[{"name":"Bacteroides","size":0.0427,"colname":"X5"}],"colname":"X4"},{"name":"Porphyromonadaceae","children":[{"name":"Porphyromonas","size":0.0128,"colname":"X5"}],"colname":"X4"},{"name":"Prevotellaceae","children":[{"name":"Alloprevotella","size":0.0372,"colname":"X5"},{"name":"Prevotella","size":0.0334,"colname":"X5"},{"name":"Prevotella_7","size":0.0213,"colname":"X5"}],"colname":"X4"},{"name":"Rikenellaceae","children":[{"name":"Rikenellaceae RC9 gut group","size":0.1346,"colname":"X5"},{"name":"U29_B03","size":0.0131,"colname":"X5"}],"colname":"X4"},{"name":"Tannerellaceae","children":[{"name":"Macellibacteroides","size":0.0653,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Chitinophagales","children":[{"name":"Chitinophagaceae","children":[{"name":"Sediminibacterium","size":0.0173,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Flavobacteriales","children":[{"name":"Flavobacteriaceae","children":[{"name":"Flavobacterium","size":3.1099,"colname":"X5"},{"name":"Myroides","size":0.3609,"colname":"X5"},{"name":"Robiginitalea","size":0.0116,"colname":"X5"},{"name":"Spongiimonas","size":0.0298,"colname":"X5"}],"colname":"X4"},{"name":"Weeksellaceae","children":[{"name":"Chryseobacterium","size":2.0134,"colname":"X5"},{"name":"Cloacibacterium","size":0.0636,"colname":"X5"},{"name":"Empedobacter","size":0.4414,"colname":"X5"},{"name":"Riemerella","size":0.0093,"colname":"X5"},{"name":"Soonwooa","size":0.2171,"colname":"X5"},{"name":"unknown genus","size":0.0569,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Sphingobacteriales","children":[{"name":"Sphingobacteriaceae","children":[{"name":"Pedobacter","size":0.0062,"colname":"X5"},{"name":"Sphingobacterium","size":0.1125,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Campylobacterota","children":[{"name":"Campylobacteria","children":[{"name":"Campylobacterales","children":[{"name":"Arcobacteraceae","children":[{"name":"Arcobacter","size":0.0162,"colname":"X5"},{"name":"Pseudarcobacter","size":0.0307,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Cyanobacteria","children":[{"name":"Cyanobacteriia","children":[{"name":"Synechococcales","children":[{"name":"Cyanobiaceae","children":[{"name":"Cyanobium PCC_6307","size":0.1737,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Desulfobacterota","children":[{"name":"Desulfobacteria","children":[{"name":"Desulfobacterales","children":[{"name":"Desulfosarcinaceae","children":[{"name":"SEEP_SRB1","size":0.0166,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Desulfobulbia","children":[{"name":"Desulfobulbales","children":[{"name":"Desulfocapsaceae","children":[{"name":"unknown genus","size":0.0334,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Firmicutes","children":[{"name":"Bacilli","children":[{"name":"Bacillales","children":[{"name":"Bacillaceae","children":[{"name":"Anoxybacillus","size":0.0448,"colname":"X5"},{"name":"Bacillus","size":0.1103,"colname":"X5"},{"name":"Caldibacillus","size":0.0483,"colname":"X5"}],"colname":"X4"},{"name":"Planococcaceae","children":[{"name":"Chryseomicrobium","size":0.0134,"colname":"X5"},{"name":"Kurthia","size":0.0326,"colname":"X5"},{"name":"Planococcus","size":0.0675,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Erysipelotrichales","children":[{"name":"Erysipelatoclostridiaceae","children":[{"name":"Sharpea","size":0.0352,"colname":"X5"}],"colname":"X4"},{"name":"Erysipelotrichaceae","children":[{"name":"Erysipelothrix","size":0.0081,"colname":"X5"},{"name":"Faecalitalea","size":0.0259,"colname":"X5"},{"name":"Holdemanella","size":0.0438,"colname":"X5"},{"name":"Turicibacter","size":0.051,"colname":"X5"},{"name":"ZOR0006","size":1.7655,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Exiguobacterales","children":[{"name":"Exiguobacteraceae","children":[{"name":"Exiguobacterium","size":0.0674,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Lactobacillales","children":[{"name":"Aerococcaceae","children":[{"name":"Abiotrophia","size":0.0193,"colname":"X5"},{"name":"Aerococcus","size":0.0538,"colname":"X5"},{"name":"Facklamia","size":0.0323,"colname":"X5"},{"name":"Ignavigranum","size":0.0101,"colname":"X5"},{"name":"unknown genus","size":0.0743,"colname":"X5"}],"colname":"X4"},{"name":"Carnobacteriaceae","children":[{"name":"Atopostipes","size":0.0095,"colname":"X5"},{"name":"Carnobacterium","size":1.7918,"colname":"X5"},{"name":"Granulicatella","size":0.009,"colname":"X5"},{"name":"Jeotgalibaca","size":0.0439,"colname":"X5"},{"name":"Trichococcus","size":0.0345,"colname":"X5"}],"colname":"X4"},{"name":"Enterococcaceae","children":[{"name":"Enterococcus","size":0.3359,"colname":"X5"}],"colname":"X4"},{"name":"Lactobacillaceae","children":[{"name":"Companilactobacillus","size":0.0861,"colname":"X5"},{"name":"Dellaglioa","size":1.1364,"colname":"X5"},{"name":"Lacticaseibacillus","size":0.036,"colname":"X5"},{"name":"Lactiplantibacillus","size":0.0311,"colname":"X5"},{"name":"Lactobacillus","size":0.2276,"colname":"X5"},{"name":"Latilactobacillus","size":6.1807,"colname":"X5"},{"name":"Leuconostoc","size":4.7748,"colname":"X5"},{"name":"Levilactobacillus","size":0.0369,"colname":"X5"},{"name":"Ligilactobacillus","size":0.1269,"colname":"X5"},{"name":"Limosilactobacillus","size":0.0312,"colname":"X5"},{"name":"Paucilactobacillus","size":0.0347,"colname":"X5"},{"name":"Secundilactobacillus","size":0.012,"colname":"X5"},{"name":"Weissella","size":1.5345,"colname":"X5"}],"colname":"X4"},{"name":"Listeriaceae","children":[{"name":"Brochothrix","size":9.8222,"colname":"X5"},{"name":"Listeria","size":0.0746,"colname":"X5"}],"colname":"X4"},{"name":"Streptococcaceae","children":[{"name":"Lactococcus","size":4.4819,"colname":"X5"},{"name":"Streptococcus","size":1.6357,"colname":"X5"}],"colname":"X4"},{"name":"Vagococcaceae","children":[{"name":"Vagococcus","size":0.2352,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Mycoplasmatales","children":[{"name":"Mycoplasmataceae","children":[{"name":"Candidatus Bacilloplasma","size":0.4622,"colname":"X5"},{"name":"Mycoplasma","size":0.094,"colname":"X5"},{"name":"unknown genus","size":0.0152,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Paenibacillales","children":[{"name":"Paenibacillaceae","children":[{"name":"Paenibacillus","size":0.0425,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Staphylococcales","children":[{"name":"Gemellaceae","children":[{"name":"Gemella","size":0.0245,"colname":"X5"}],"colname":"X4"},{"name":"Staphylococcaceae","children":[{"name":"Jeotgalicoccus","size":0.0329,"colname":"X5"},{"name":"Macrococcus","size":0.5535,"colname":"X5"},{"name":"Nosocomiicoccus","size":0.0091,"colname":"X5"},{"name":"Staphylococcus","size":2.0317,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Clostridia","children":[{"name":"Christensenellales","children":[{"name":"Christensenellaceae","children":[{"name":"Christensenellaceae R_7 group","size":0.054,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Clostridia UCG_014","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0558,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Clostridiales","children":[{"name":"Clostridiaceae","children":[{"name":"Clostridium sensu stricto 1","size":0.0784,"colname":"X5"},{"name":"Clostridium sensu stricto 5","size":0.0069,"colname":"X5"},{"name":"Proteiniclasticum","size":0.0615,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Eubacteriales","children":[{"name":"Eubacteriaceae","children":[{"name":"Eubacterium","size":0.0078,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Lachnospirales","children":[{"name":"Lachnospiraceae","children":[{"name":"[Ruminococcus] gauvreauii group","size":0.0398,"colname":"X5"},{"name":"Blautia","size":0.0571,"colname":"X5"},{"name":"Butyrivibrio","size":0.0097,"colname":"X5"},{"name":"Dorea","size":0.0108,"colname":"X5"},{"name":"Lachnoclostridium","size":0.023,"colname":"X5"},{"name":"Lachnospiraceae NK3A20 group","size":0.0649,"colname":"X5"},{"name":"Syntrophococcus","size":0.0102,"colname":"X5"},{"name":"Tyzzerella","size":0.0691,"colname":"X5"},{"name":"unknown genus","size":0.0173,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Oscillospirales","children":[{"name":"Hungateiclostridiaceae","children":[{"name":"Saccharofermentans","size":0.019,"colname":"X5"}],"colname":"X4"},{"name":"Oscillospiraceae","children":[{"name":"UCG_005","size":0.0128,"colname":"X5"}],"colname":"X4"},{"name":"Ruminococcaceae","children":[{"name":"Faecalibacterium","size":0.0756,"colname":"X5"},{"name":"Ruminococcus","size":0.0136,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Peptostreptococcales_Tissierellales","children":[{"name":"Anaerovoracaceae","children":[{"name":"Mogibacterium","size":0.0084,"colname":"X5"}],"colname":"X4"},{"name":"Family XI","children":[{"name":"Anaerococcus","size":0.0202,"colname":"X5"},{"name":"Anaerosphaera","size":0.074,"colname":"X5"},{"name":"Finegoldia","size":0.0142,"colname":"X5"},{"name":"Gallicola","size":0.0182,"colname":"X5"},{"name":"Peptoniphilus","size":0.0933,"colname":"X5"},{"name":"Tissierella","size":0.0122,"colname":"X5"}],"colname":"X4"},{"name":"Peptostreptococcaceae","children":[{"name":"Intestinibacter","size":0.0253,"colname":"X5"},{"name":"Paraclostridium","size":0.0115,"colname":"X5"},{"name":"Peptostreptococcus","size":0.0409,"colname":"X5"},{"name":"Proteocatella","size":0.0037,"colname":"X5"},{"name":"unknown genus","size":0.1273,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Negativicutes","children":[{"name":"Acidaminococcales","children":[{"name":"Acidaminococcaceae","children":[{"name":"Succiniclasticum","size":0.0372,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Veillonellales_Selenomonadales","children":[{"name":"Veillonellaceae","children":[{"name":"Veillonella","size":0.0281,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Fusobacteriota","children":[{"name":"Fusobacteriia","children":[{"name":"Fusobacteriales","children":[{"name":"Fusobacteriaceae","children":[{"name":"Cetobacterium","size":0.2577,"colname":"X5"},{"name":"Fusobacterium","size":0.1245,"colname":"X5"}],"colname":"X4"},{"name":"Leptotrichiaceae","children":[{"name":"Hypnocyclicus","size":0.4276,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Patescibacteria","children":[{"name":"Gracilibacteria","children":[{"name":"Candidatus Peribacteria","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0146,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Saccharimonadia","children":[{"name":"Saccharimonadales","children":[{"name":"Saccharimonadaceae","children":[{"name":"Candidatus Saccharimonas","size":0.0195,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Proteobacteria","children":[{"name":"Alphaproteobacteria","children":[{"name":"Acetobacterales","children":[{"name":"Acetobacteraceae","children":[{"name":"Acidocella","size":0.0149,"colname":"X5"},{"name":"Roseomonas","size":0.1252,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Caulobacterales","children":[{"name":"Caulobacteraceae","children":[{"name":"Brevundimonas","size":0.1044,"colname":"X5"},{"name":"unknown genus","size":0.01,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Rhizobiales","children":[{"name":"Beijerinckiaceae","children":[{"name":"Methylobacterium_Methylorubrum","size":0.0316,"colname":"X5"}],"colname":"X4"},{"name":"Pleomorphomonadaceae","children":[{"name":"Pleomorphomonas","size":0.0161,"colname":"X5"}],"colname":"X4"},{"name":"Rhizobiaceae","children":[{"name":"Allorhizobium_Neorhizobium_Pararhizobium_Rhizobium","size":0.0438,"colname":"X5"},{"name":"Ochrobactrum","size":0.1527,"colname":"X5"},{"name":"Pseudochrobactrum","size":0.24,"colname":"X5"}],"colname":"X4"},{"name":"Stappiaceae","children":[{"name":"Roseibium","size":0.0076,"colname":"X5"}],"colname":"X4"},{"name":"Xanthobacteraceae","children":[{"name":"unknown genus","size":0.0814,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Rhodobacterales","children":[{"name":"Rhodobacteraceae","children":[{"name":"Paracoccus","size":0.1927,"colname":"X5"},{"name":"Rhodobacter","size":0.0178,"colname":"X5"},{"name":"Ruegeria","size":0.0412,"colname":"X5"},{"name":"Silicimonas","size":0.0096,"colname":"X5"},{"name":"Tabrizicola","size":0.0162,"colname":"X5"},{"name":"Tropicimonas","size":0.0296,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Sphingomonadales","children":[{"name":"Sphingomonadaceae","children":[{"name":"Rhizorhapis","size":0.0201,"colname":"X5"},{"name":"Sphingobium","size":0.0121,"colname":"X5"},{"name":"Sphingomonas","size":0.0383,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"unknown order","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0608,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"},{"name":"Gammaproteobacteria","children":[{"name":"Burkholderiales","children":[{"name":"Burkholderiaceae","children":[{"name":"Burkholderia_Caballeronia_Paraburkholderia","size":0.0229,"colname":"X5"},{"name":"Limnobacter","size":0.072,"colname":"X5"},{"name":"Ralstonia","size":0.0178,"colname":"X5"}],"colname":"X4"},{"name":"Chitinimonadaceae","children":[{"name":"Chitinimonas","size":0.0141,"colname":"X5"}],"colname":"X4"},{"name":"Comamonadaceae","children":[{"name":"Acidovorax","size":0.086,"colname":"X5"},{"name":"Aquabacterium","size":0.0318,"colname":"X5"},{"name":"Comamonas","size":0.2547,"colname":"X5"},{"name":"Curvibacter","size":0.015,"colname":"X5"},{"name":"Delftia","size":0.0176,"colname":"X5"},{"name":"Limnohabitans","size":0.0208,"colname":"X5"},{"name":"Pelomonas","size":0.0361,"colname":"X5"},{"name":"Polaromonas","size":0.0086,"colname":"X5"},{"name":"Pseudorhodoferax","size":0.0088,"colname":"X5"},{"name":"Rhodoferax","size":0.0057,"colname":"X5"},{"name":"Schlegelella","size":0.0133,"colname":"X5"},{"name":"Simplicispira","size":0.013,"colname":"X5"},{"name":"unknown genus","size":0.0716,"colname":"X5"},{"name":"Variovorax","size":0.0104,"colname":"X5"},{"name":"Xylophilus","size":0.0061,"colname":"X5"}],"colname":"X4"},{"name":"Hydrogenophilaceae","children":[{"name":"Hydrogenophilus","size":0.3541,"colname":"X5"}],"colname":"X4"},{"name":"Neisseriaceae","children":[{"name":"Neisseria","size":0.0254,"colname":"X5"},{"name":"unknown genus","size":0.2024,"colname":"X5"},{"name":"Uruburuella","size":0.0262,"colname":"X5"},{"name":"Vitreoscilla","size":0.0049,"colname":"X5"}],"colname":"X4"},{"name":"Oxalobacteraceae","children":[{"name":"[Aquaspirillum] arcticum group","size":0.1715,"colname":"X5"},{"name":"Janthinobacterium","size":0.4745,"colname":"X5"},{"name":"Massilia","size":0.059,"colname":"X5"},{"name":"Undibacterium","size":0.0357,"colname":"X5"}],"colname":"X4"},{"name":"Rhodocyclaceae","children":[{"name":"Methyloversatilis","size":0.0083,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Chromatiales","children":[{"name":"Chromatiaceae","children":[{"name":"unknown genus","size":0.0096,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Enterobacterales","children":[{"name":"Aeromonadaceae","children":[{"name":"Aeromonas","size":0.0081,"colname":"X5"}],"colname":"X4"},{"name":"Enterobacteriaceae","children":[{"name":"Raoultella","size":0.0041,"colname":"X5"}],"colname":"X4"},{"name":"Hafniaceae","children":[{"name":"Hafnia_Obesumbacterium","size":0.0309,"colname":"X5"}],"colname":"X4"},{"name":"Morganellaceae","children":[{"name":"Morganella","size":0.0035,"colname":"X5"},{"name":"Proteus","size":0.0285,"colname":"X5"}],"colname":"X4"},{"name":"Pseudoalteromonadaceae","children":[{"name":"Pseudoalteromonas","size":0.0148,"colname":"X5"}],"colname":"X4"},{"name":"Shewanellaceae","children":[{"name":"Shewanella","size":0.1157,"colname":"X5"}],"colname":"X4"},{"name":"Vibrionaceae","children":[{"name":"Aliivibrio","size":0.1562,"colname":"X5"},{"name":"Photobacterium","size":5.1659,"colname":"X5"},{"name":"Vibrio","size":0.0395,"colname":"X5"}],"colname":"X4"},{"name":"Yersiniaceae","children":[{"name":"Serratia","size":0.1744,"colname":"X5"},{"name":"Yersinia","size":0.0166,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Milano_WF1B_44","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.1134,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Pseudomonadales","children":[{"name":"Moraxellaceae","children":[{"name":"Acinetobacter","size":0.261,"colname":"X5"},{"name":"Psychrobacter","size":0.5081,"colname":"X5"}],"colname":"X4"},{"name":"Pseudomonadaceae","children":[{"name":"Pseudomonas","size":0.5068,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"TDNP_Wbc97_11_7_16","children":[{"name":"unknown family","children":[{"name":"unknown genus","size":0.0305,"colname":"X5"}],"colname":"X4"}],"colname":"X3"},{"name":"Xanthomonadales","children":[{"name":"Rhodanobacteraceae","children":[{"name":"Dyella","size":0.0362,"colname":"X5"},{"name":"Fulvimonas","size":0.1291,"colname":"X5"},{"name":"Rhodanobacter","size":0.0857,"colname":"X5"}],"colname":"X4"},{"name":"Xanthomonadaceae","children":[{"name":"Lysobacter","size":0.0076,"colname":"X5"},{"name":"Stenotrophomonas","size":0.1953,"colname":"X5"},{"name":"Xylella","size":0.0165,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"},{"name":"Spirochaetota","children":[{"name":"Spirochaetia","children":[{"name":"Spirochaetales","children":[{"name":"Spirochaetaceae","children":[{"name":"unknown genus","size":0.062,"colname":"X5"}],"colname":"X4"}],"colname":"X3"}],"colname":"X2"}],"colname":"X1"}],"name":"root"},"tooltip":null,"breadcrumbs":null},"options":{"colors":null,"valueField":"size","rootLabel":null,"showLabels":false}},"evals":[],"jsHooks":[]}</script>
```

## Tree with ggtree package


```r
data_tree <- phy_tree(data_genus)
data_tree |> 
  ggtree(layout = "circular")
```

![](06_taxonomy_files/figure-html/unnamed-chunk-12-1.png)<!-- -->







