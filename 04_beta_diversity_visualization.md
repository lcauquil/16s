---
title: "Beta Diversity visualization"
author: "Laurent Cauquil"
output: 
  html_document:
    keep_md: yes
    toc: TRUE
    toc_float: TRUE
---




```r
library(dplyr)
library(tibble)
library(ggplot2)
library(phyloseq)
library(dendextend)
library(colorspace)
library(vegan)
```


## Phyloseq object

Let's import a phyloseq object in the workspace


```r
load("data/16S_phyloseq.rdata")
ls()
```

```
## [1] "data"
```

Let's take a look a the data


```r
data
```

```
## phyloseq-class experiment-level object
## otu_table()   OTU Table:         [ 498 taxa and 64 samples ]
## sample_data() Sample Data:       [ 64 samples by 4 sample variables ]
## tax_table()   Taxonomy Table:    [ 498 taxa by 7 taxonomic ranks ]
## phy_tree()    Phylogenetic Tree: [ 498 tips and 497 internal nodes ]
```

## Clustering ordination

### Unifrac distance and Ward linkage


```r
data_raref <- rarefy_even_depth(data, rngseed = 12345)
```

```
## `set.seed(12345)` was used to initialize repeatable random subsampling.
```

```
## Please record this for your records so others can reproduce.
```

```
## Try `set.seed(12345); .Random.seed` for the full vector
```

```
## ...
```

```r
data_raref |> 
  distance("unifrac") |> 
  hclust("ward.D2") |> 
  as.dendrogram() -> tmp

envtype <- levels(sample_data(data)$EnvType)
envtype_col <- c('#970007','#d44d23','#cd7e41','#deb070','#d0e3e8','#9bc9d9','#649dc1','#255594')

## assign colors to label of the dendrogram
labels_colors(tmp) <- envtype_col[sort_levels_values(as.numeric(sample_data(data_raref)$EnvType)[order.dendrogram(tmp)])]

plot(tmp, main = "Hierarchical Clustering")
legend("topright", 
       legend = envtype, 
       fill = envtype_col, 
       border = envtype_col, 
       box.col = "white",
       cex = .75,
       y.intersp = 1, ## space between boxes and labels
       ncol = 2)
```

![](04_beta_diversity_visualization_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

```r
rm(tmp)
```

## MDS/PCoA ordination


```r
tmp <- metaMDS(distance(data, method = "bray"), k=3)
```

```
## Run 0 stress 0.1251792 
## Run 1 stress 0.1251793 
## ... Procrustes: rmse 0.000622097  max resid 0.002700925 
## ... Similar to previous best
## Run 2 stress 0.1251944 
## ... Procrustes: rmse 0.003714058  max resid 0.02696678 
## Run 3 stress 0.1274604 
## Run 4 stress 0.1259572 
## Run 5 stress 0.1251945 
## ... Procrustes: rmse 0.003691381  max resid 0.02711173 
## Run 6 stress 0.1274601 
## Run 7 stress 0.1251946 
## ... Procrustes: rmse 0.003709138  max resid 0.02714632 
## Run 8 stress 0.1251792 
## ... New best solution
## ... Procrustes: rmse 0.0003103007  max resid 0.001929336 
## ... Similar to previous best
## Run 9 stress 0.1274603 
## Run 10 stress 0.1274602 
## Run 11 stress 0.125179 
## ... New best solution
## ... Procrustes: rmse 0.0005551015  max resid 0.002225522 
## ... Similar to previous best
## Run 12 stress 0.1251941 
## ... Procrustes: rmse 0.003753743  max resid 0.02734592 
## Run 13 stress 0.1251791 
## ... Procrustes: rmse 0.0002413345  max resid 0.001337443 
## ... Similar to previous best
## Run 14 stress 0.1251791 
## ... Procrustes: rmse 0.0005716602  max resid 0.001833213 
## ... Similar to previous best
## Run 15 stress 0.1251791 
## ... Procrustes: rmse 0.0005438331  max resid 0.002504824 
## ... Similar to previous best
## Run 16 stress 0.1272225 
## Run 17 stress 0.1274606 
## Run 18 stress 0.1251793 
## ... Procrustes: rmse 0.0006211284  max resid 0.002441528 
## ... Similar to previous best
## Run 19 stress 0.1271879 
## Run 20 stress 0.1251789 
## ... New best solution
## ... Procrustes: rmse 0.0001006996  max resid 0.000581784 
## ... Similar to previous best
## *** Solution reached
```

```r
tmp2 <- rownames_to_column(data.frame(tmp$points[,1:2]))
  
  ggplot(data.frame(tmp2), aes(x = MDS1, y = MDS2, text = rowname)) +
    geom_point(aes(col = sample_data(data)$EnvType), size = 4, alpha = 0.8) + 
    labs(title = "nMDS with Bray distance", 
         subtitle = paste0("Stress = ", round(tmp$stress, 3))) +
    labs(colour = "EnvType") +
    theme_bw() + 
    theme(axis.line=element_line(size = 1, color = "black"), 
          axis.ticks.length = unit(.2, "cm"), 
          axis.text = element_text(size = 12, color = "black"), 
          axis.title = element_text(size = 17, color = "black"),
          axis.title.x = element_blank(),  
          axis.title.y = element_blank()) +
    theme(aspect.ratio = 1)   ## square shape
```

![](04_beta_diversity_visualization_files/figure-html/unnamed-chunk-5-1.png)<!-- -->



